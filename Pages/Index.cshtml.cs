﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Web;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Hyponet_UI.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IWebHostEnvironment webHostEnvironment;


        public IndexModel(ILogger<IndexModel> logger, IWebHostEnvironment webHostEnvironment)
        {
            _logger = logger;

            this.webHostEnvironment = webHostEnvironment;

        }

        public void OnGet()
        {

        }

        /// <summary>  
        /// Lægger filer fra filsystemet på serveren
        /// </summary>  
        /// <param name="MyUploader">De filer der sendes</param> 
        /// <returns>succes eller fejl</returns>  
        public IActionResult OnPostMyUploader(IFormFile MyUploader)
        {
            if (MyUploader != null)
            {
                //Filnavn på den valgte fil sammen med en random string og filtypen
               // string ImageFileName = Path.GetFileName(MyUploader.FileName) + "-" + Guid.NewGuid() + Path.GetExtension(MyUploader.FileName);

                string uploadsFolder = Path.Combine(webHostEnvironment.WebRootPath, "upload");
                string filePath = Path.Combine(uploadsFolder, MyUploader.FileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    MyUploader.CopyTo(fileStream);
                }
                return new ObjectResult(new { status = "success" });
            }
            return new ObjectResult(new { status = "fail" });
            // razor upload end

        }


    }
}





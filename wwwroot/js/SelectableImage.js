﻿/**
 * An image that allows for selection using canvas.
 * Custom event fires on "imageselect" when a selection is drawn.
 * 
 * existingSelection parameter can be used to display a selection on the image at creation, disabling regular selection.
 * @module SelectableImage
 */
class SelectableImage {
    /** @param {HTMLImageElement} image 
     *  @param {{startPos: {x: number, y: number}, endPos: {x: number, y: number}}} existingSelection 
     */
    constructor(image, existingSelection = null) {
        this.smallestSize = 20;

        this.img = image
        const selectableImage = this

        if (existingSelection === null)
            selectableImage.img.addEventListener("mousedown", HandleMouseDown)
        else {
            selectableImage.img.addEventListener("load", HandleLoad)
        }

        function HandleLoad(e) {
            setTimeout(() => {
                const { canvas, ctx } = selectableImage.ConvertToCanvas()
                selectableImage.HighlightSelection(canvas, ctx, existingSelection.startPos, existingSelection.endPos)
                selectableImage.ConvertToImage(canvas)
            }, 1000)
        }

        function HandleMouseDown(e) {
            const { canvas, ctx } = selectableImage.ConvertToCanvas()
            selectableImage.canvas = canvas
            selectableImage.ctx = ctx
            selectableImage.startPos = { x: 0, y: 0 }

            canvas.addEventListener("mouseenter", HandleMouseEnter)
            canvas.addEventListener("mousemove", HandleMouseMove)
            canvas.addEventListener("mouseup", HandleMouseUp)
        }

        function HandleMouseEnter(e) {
            selectableImage.startPos = { x: e.clientX - e.target.getBoundingClientRect().x, y: e.clientY - e.target.getBoundingClientRect().y }
            e.target.removeEventListener("mouseenter", HandleMouseEnter)
        }

        function HandleMouseMove(e) {
            const targetX = e.clientX - e.target.getBoundingClientRect().x - selectableImage.startPos.x
            const targetY = e.clientY - e.target.getBoundingClientRect().y - selectableImage.startPos.y
            const isValidSize = !(Math.abs(targetX) < selectableImage.smallestSize || Math.abs(targetY) < selectableImage.smallestSize)

            selectableImage.HighlightSelection(selectableImage.canvas, selectableImage.ctx, selectableImage.startPos, { x: targetX, y: targetY }, isValidSize ? "red" : "yellow")
        }

        function HandleMouseUp(e) {

            const endPos = { x: e.clientX - e.target.getBoundingClientRect().x, y: e.clientY - e.target.getBoundingClientRect().y }
            if (Math.abs(selectableImage.startPos.x - endPos.x) < selectableImage.smallestSize || Math.abs(selectableImage.startPos.y - endPos.y) < selectableImage.smallestSize)
                return

            selectableImage.Select(selectableImage.canvas, selectableImage.startPos, endPos)
            selectableImage.ConvertToImage(selectableImage.canvas)
            selectableImage.img.removeEventListener("mousedown", HandleMouseDown)


            e.target.removeEventListener("mouseup", HandleMouseUp)
            e.target.removeEventListener("mousemove", HandleMouseMove)
        }
    }

    /** Creates a new canvas using the img. Hides the img with CSS.
     * @returns {HTMLCanvasElement}
     */
    ConvertToCanvas() {
        const canvas = document.createElement("canvas")
        canvas.width = this.img.width
        canvas.height = this.img.height
        this.img.style.display = "none"
        const imgContainer = this.img.parentElement
        console.log(imgContainer)

        const ctx = canvas.getContext("2d")
        ctx.drawImage(this.img, 0, 0, canvas.width, canvas.height)
        imgContainer.appendChild(canvas)

        return { canvas, ctx }
    }

    /** Puts the canvas image into the img and unhides it. Deletes the canvas.
     * @param {HTMLCanvasElement} canvas
     */
    ConvertToImage(canvas) {
        this.img.src = canvas.toDataURL()
        this.img.style.display = "block"

        canvas.remove()
    }

    /** Create a box highlight on the canvas
     * @param {HTMLCanvasElement} canvas
     * @param {CanvasRenderingContext2D} ctx
     * @param {{x: number, y: number}} startPos
     * @param {{x: number, y: number}} endPos
     */
    HighlightSelection(canvas, ctx, startPos, endPos, strokeStyle = "red") {

        ctx.drawImage(this.img, 0, 0, canvas.width, canvas.height)

        ctx.strokeStyle = strokeStyle

        ctx.strokeRect(
            startPos.x,
            startPos.y,
            endPos.x,
            endPos.y
        )
    }

    /** Returns cropped image within selection as base64 string
     * Also triggers custom event on "imageselect" containing the base64 string in event.detail
     * @param {HTMLCanvasElement} canvas
     * @param {{x: number, y: number}} startPos
     * @param {{x: number, y: number}} endPos
     * @returns string 
     */
    Select(canvas, startPos, endPos) {
        const croppedCanvas = document.createElement("canvas")
        croppedCanvas.width = canvas.width
        croppedCanvas.height = canvas.height

        const cropctx = croppedCanvas.getContext("2d")
        cropctx.drawImage(this.img, 0, 0, canvas.width, canvas.height)
        SelectableImage.CropCanvasToArea(croppedCanvas, startPos, endPos)

        const data = croppedCanvas.toDataURL()
        const event = new CustomEvent("imageselect", { detail: data });
        this.img.dispatchEvent(event)

        return data
    }

    /** Crops the canvas.
     * @param {HTMLCanvasElement} canvas 
     * @param {{x: number, y: number}} a
     * @param {{x: number, y: number}} b
     */
    static CropCanvasToArea(canvas, a, b) {
        if (a.x > b.x) [a.x, b.x] = [b.x, a.x]
        if (a.y > b.y) [a.y, b.y] = [b.y, a.y]

        let ctx = canvas.getContext('2d');
        let imageData = ctx.getImageData(a.x, a.y, b.x, b.y);
        canvas.width = b.x - a.x
        canvas.height = b.y - a.y
        ctx.putImageData(imageData, 0, 0)
    }

}
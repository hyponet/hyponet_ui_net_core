///Files

//laver en streng med alle filnavnene (fx pik.jpg \n kusse.jpg)
    //function getFileNames(files) {
    //    let MyFiles = "";

    //    for (let i = 0; i < files.length; i++) {
    //        MyFiles = MyFiles + files[i].name + "\n";
    //    }
    //    return MyFiles

    //}

//Lav en tilf�ldig streng til brug i filupload
function makeId(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}

function renameFile(file) {

    //Lav en tilf�ldig streng p� 5 tegn
    var random = makeId(5);

    var fileArray = file.name.split(".");
    //billedet hedder: orginalt filnavn + tilf�ldig streng + .filtype
    try {
        var newFileName = fileArray[0] + random + "." + fileArray[1];
        var newFile = new File(Array(file), newFileName, {
            type: "image/" + fileArray[1],
        });
    } catch (error) { console.error(error); }
    finally {
        return newFile;
    }

}

function processFile(event, file) {

    //tilf�jer billedet til den aktive box i dommen
    //Den skal gentages for hver billede i inputtet
    //der skal laves en ny r�d boks for hver(ny kontekst)

    var image = event.target.append(img_create(file.name));
    prependRedBox(image);

}


//Henter filer til upload-mappe via input-tag i bunden af siden
function uploadFiles(event) {


    //put dataTransfer.files ind i input-tag
    $("#fileUpload").prop("files", event.dataTransfer.files);
    var files = $("#fileUpload").prop("files");

    if (files.length > 0) {

        let filesCount = 0;
        let url = "/Index?handler=MyUploader";
        let formData;
        var MyFilesArray = new Array();

        for (let arrayIndex = 0; arrayIndex < files.length; arrayIndex++) {

            //Alle filer omd�bes til: filnavnet + 5 tilf�ldige tegn + .filtype
            MyFilesArray[arrayIndex] = renameFile(files[arrayIndex]);

            //Upload multiple files p� samme tid via formdata
            if (MyFilesArray.length > 0) {

                formData = new FormData();
                formData.append("MyUploader", MyFilesArray[arrayIndex]);



                jQuery.ajax({
                    type: 'POST',
                    url: url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function (xhr) {

                        xhr.setRequestHeader("XSRF-TOKEN",

                            $("#__RequestVerificationToken").attr('value'));

                    },

                    //hvis filerne er uploadet
                    success: function (repo) {
                        if (repo.status == "success") {

                            //prepend er asynkron, derfor skal tilf�jelse af billeder ske inde i et ajax-kald

                            processFile(event, MyFilesArray[arrayIndex]);

                            filesCount++;

                            if (filesCount < files.length) {

                                //console.log(filesCount + " Files : " + getFileNames(MyFilesArray) + " uploaded successfully");                
                                return MyFilesArray;
                            }
                        }
                    },
                    error: function (e) {
                        console.error("Fejl i billedeupload");
                    }
                });
            }
            else {
                console.error("No files to upload in MyFilesArray!");
            }



        }
    }
    else { console.error("Error: files.length is not > 0: " + files.length); }




    return MyFilesArray;
}


//Upload billeder sv�bt i Promise
function getPromise(event) {
    var promise = new Promise(async function (resolve) {
        //eventet har dataTransfer.files i sig, som bruges her i en formdata-forsendelse
        var result = uploadFiles(event);
        resolve(result);
    });

    return promise;
}

//Tjekker om indholdet af p-tag slutter p� .jpg eller andre filnavne
function checkForImage(node) {

    var extension = node.nodeName.substr((node.nodeName.lastIndexOf('.') + 1));

    switch (extension) {
        case 'jpg':
            return img_create(node.nodeName);
            break;
        case 'wepb':
        case 'jpeg':
            return img_create(node.nodeName);
            break;
        case 'png':
            return img_create(node.nodeName);
            break;
        case 'gif':
            return img_create(node.nodeName);
            break;
        case 'zip':
        case 'rar':
        //alert('was zip rar');
        //break;
        case 'pdf':
        //alert('was pdf');
        //break;            
    }

    return node.nodeName;
}

function testFileName(fileName) {

    const regEx = /^[\w,\s-]+\.[A-Za-z]{3}$/gm
    if (regEx.test(fileName)) {
        console.log("Valid filename: " + fileName);
        return true;
    }
    else {
        console.error("Invalid filename: " + fileName);
    }
    return false;
}

//Opretter billedet via API'et p� Neo4J'
async function createImage(e, nodeType, files) {
    var method = "POST";
    for (var i = 0; i < files.length; i++) {
        var requestParameters = {
            "fileName": files[i].name,
            "type": nodeType
        };
        var apiEndpointUrl = _baseUrl + "Node/CreateImage";
        var nodeResult = await SendRequest(apiEndpointUrl, requestParameters, method, e.currentTarget);
    }
    return nodeResult;
}

//Tilf�jer Img-tag til HTML-dommen
function img_create(src, alt, title) {

    // load and draw the main image on the canvas
    //opretter et IMG-tag
    var img = new Image();
    //var img = new Image(document.createElement('img'));
    img.setAttribute("height", "10%");
    img.setAttribute("width", "100%");

    //Forhindrer at man "kopierer" billedet n�r man fors�ger at markere p� billedet
    img.setAttribute("draggable", false);

    //Hvad g�r crossOrigin og onload?
    img.crossOrigin = "anonymous";


    img.src = "upload/" + src;
    if (alt != null) img.alt = alt;
    if (title != null) img.title = title;


    return img;
}
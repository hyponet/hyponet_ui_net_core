//Tolk gr�nne bokse som SPEC og r�de bokse som ROOT, selvom de ikke er oprettet i neo4j endnu
function getType(activePTag) {
    console.log(activePTag);
    var returnType = "Invalid Type";
    if ($(activePTag).hasClass("redbox")) { returnType = "ROOT"; }
    if ($(activePTag).hasClass("greenbox")) { returnType = "SPEC"; }
    console.log("returnType: " + returnType);
    return returnType;
}

//s�t ID p� box i UI
function setBoxId(resultObject) {
    $(resultObject.element).attr("id", resultObject.node[0].nodeId);
    $(resultObject.element).attr("data-nodeId", resultObject.node[0].nodeId);
    return resultObject.element.getAttribute("data-nodeId");
}

//Opretter bokse i UI
function createNode(node) {

    if (node.nodeLabel != "MARK") {
        let newbox = document.createElement("p");
        $(newbox).hide();
        $(newbox).attr("data-nodeid", node.nodeId);
        $(newbox).html(checkForImage(node));
        $(newbox).attr("contenteditable", "false");

        if (node.nodeLabel == "ROOT") {
            $(newbox).addClass("redbox");
            //Markering i redbox
            newbox.addEventListener("mouseup", (e) => {
                console.log("Selected redbox text")
                //Marker i boksen, hvis den ikke er tom
                if (e.target.innerHTML != "") {
                    //hvis det ikke er et billede
                    if (!$(e.target).find('img').length) {
                        selectTextFromWindow(e);
                        console.log("Det er ikke et billede")
                    }
                    //hvis der er et billede i boksen
                    else {
                        selectImageFromWindow(e)
                        console.log("Det er et billede")
                    }
                }

                //Fjern tom greenbox
                if (e.currentTarget.nextElementSibling != null && e.currentTarget.nextElementSibling.childNodes != null && e.currentTarget.nextElementSibling.childNodes[0].parentElement.className != "bluebox") {


                    if (e.currentTarget.nextElementSibling.childNodes[1]) {

                        if (e.currentTarget.nextElementSibling.childNodes[1].innerText === "") {
                            e.currentTarget.nextElementSibling.remove();
                        }

                    }
                }

            });
            newbox.addEventListener('click', async (e) => LastClicked(e))
        }
        else if (node.nodeLabel == "SPEC") {
            $(newbox).addClass("greenbox");
            newbox.addEventListener("mouseup", (e) => {
                if (window.getSelection().toString() !== "") {
                    console.log("ran")
                    selectTextFromWindow(e);
                }
            });
            newbox.addEventListener('click', async (e) => LastClicked(e))
        }
        else if (node.nodeLabel == "ASS") {
            $(newbox).addClass("purplebox");
        }
        document.getElementById("samtale").prepend(newbox);
        $(newbox).show("slow");

    }

    if (node.nodeLabel == "MARK") {

        //Find alle ROOT og SPEC
        var redboxes = document.getElementsByClassName("redbox");
        var greenboxes = document.getElementsByClassName("greenbox");

        for (var i = 0; i < redboxes.length; i++) {

            if (redboxes[i].innerText.includes(node.nodeName)) {

                $(redboxes[i]).html(function (_, html) {
                    return html.replace(node.nodeName, `<mark id="${node.nodeId} data-nodeId="${node.nodeId}">${node.nodeName}</mark>`);
                });

            }
        }

        for (var i = 0; i < greenboxes.length; i++) {

            if (greenboxes[i].innerText.includes(node.nodeName)) {

                $(greenboxes[i]).html(function (_, html) {
                    return html.replace(node.nodeName, `<mark id="${node.nodeId}" data-nodeId="${node.nodeId}">${node.nodeName}</mark>`);
                });

            }
        }

    }

}

function createRedBox() {
    const redBox = document.createElement("p");
    redBox.contentEditable = "true";
    redBox.classList.add("redbox");


    //Create root node when the box is unfocused if the content has changed
    async function CreateRootNode(e) {
        //Fjern eventuelle mark-elementer i dette p-element
        if (!e.currentTarget.childNodes[0] == 'img') {
            fjernMarkTag(e);
        }

        if (e.currentTarget.innerText != "" && !e.currentTarget.getAttribute("data-nodeid")) {
            await rootNodeCreation(e);
            appendRedBox()
        }
    }
    redBox.addEventListener("focusout", async (e) => CreateRootNode(e));
    redBox.addEventListener("selectstart", async (e) => CreateRootNode(e));

    redBox.addEventListener("keypress", (e) => {
        //!!Allow linebreak on enter if shift is held (linebreaks clash with API, commented for now)
        //if (e.shiftKey) return;
        if (e.key === "Enter") {
            e.preventDefault()
            e.target.blur()
        }
    })

    //Markering i redbox
    redBox.addEventListener("mouseup", (e) => {
        console.log("Selected redbox text")
        //Marker i boksen, hvis den ikke er tom
        if (e.target.innerHTML != "") {
            //hvis det ikke er et billede
            if (!$(e.target).find('img').length) {
                selectTextFromWindow(e);
                console.log("Det er ikke et billede")
            }
            //hvis der er et billede i boksen
            else {
                selectImageFromWindow(e)
                console.log("Det er et billede")
            }

        }

        //Fjern tom greenbox
        if (e.currentTarget.nextElementSibling != null && e.currentTarget.nextElementSibling.childNodes != null && e.currentTarget.nextElementSibling.childNodes[0].parentElement.className != "bluebox") {


            if (e.currentTarget.nextElementSibling.childNodes[1]) {

                if (e.currentTarget.nextElementSibling.childNodes[1].innerText === "") {
                    e.currentTarget.nextElementSibling.remove();
                }

            }
        }

    });

    //N�r musen holdes nede og k�res hen over p-tag
    redBox.addEventListener("dragover", (e) => {

        const isThisPContenteditable = e.target.isContentEditable;
        e.preventDefault();

        //tilf�j klasse til p-tag hvis det er redigerbart
        if (isThisPContenteditable)
            e.target.classList.add('dragging');
    });

    //n�r musen forlader p-tag
    redBox.addEventListener("dragleave", (e) => {

        const isThisPContenteditable = e.target.isContentEditable;
        e.preventDefault();

        //fjern klasse fra p-tag hvis det er redigerbart
        if (isThisPContenteditable)
            e.target.classList.remove('dragging');
    });

    redBox.addEventListener("drop", (e) => {

        const isThisPContenteditable = e.target.isContentEditable;
        e.preventDefault();

        if (isThisPContenteditable) {

            //Returnerer ROOT som type hvis .redbox eller SPEC hvis .greenbox
            const nodeType = getType(e.target); //Always returns string "ROOT"

            //myPromise = alle billederne, omd�bt  og uploadet
            const myPromise = getPromise(e, e.target);
            myPromise.then(async (result) => {
                //billedet sendes til neo4j 
                const resultObject = await createImage(e, nodeType, result);

                //Overvej om img.id m�ske skulle s�ttes her med setBoxId

                //!!No relationship or ass node is created for images at the moment. "markNode" is undefined in this context
            }).catch(function (error) {
                console.error(error);
            });

            //Efter drop skal class fjernes
            e.target.classList.remove('dragging');
        }
    });

    redBox.addEventListener('click', async (e) => LastClicked(e))

    return redBox
}

//Opret r�d boks
function appendRedBox() {

    const redBox = createRedBox();

    document.getElementById("samtale").append(redBox);
}

//inds�t tom redbox �verst
function prependRedBox(image) {

    const redBox = createRedBox();

    document.getElementById("samtale").prepend(redBox);

    //!!Below code appears to do nothing. There is no noticable change when removing it.
    //Inds�t et img-tag 
    if (image) {
        document.getElementById("samtale").prepend(image);
    }

    //Genlyt p� keypress osv
}

//Opret gr�n boks
function createGreenBox(domElement, resultObject) {

    const greenBox = document.createElement("p");
    greenBox.contentEditable = "true";
    greenBox.classList.add("greenbox");


    async function CreateSpecNode(e) {
        fjernMarkTag(e);
        if (e.target.innerText != "" && !e.currentTarget.getAttribute("data-nodeId")) {
            const localResultObject = await specNodeCreation(e, resultObject);
            assNodeCreation(localResultObject, resultObject);
            e.target.contentEditable = false;
        }
    }
    greenBox.addEventListener("focusout", async (e) => CreateSpecNode(e));
    greenBox.addEventListener("selectstart", async (e) => CreateSpecNode(e));

    greenBox.addEventListener("keypress", async (e) => {

        if (e.key === "Enter" && !e.currentTarget.getAttribute("data-nodeId")) {
            e.preventDefault();
            const localResultObject = await specNodeCreation(e, resultObject);
            assNodeCreation(localResultObject, resultObject);
            e.target.contentEditable = false;
        }

    });

    greenBox.addEventListener("mouseup", (e) => {
        if (window.getSelection().toString() !== "") {
            console.log("ran")
            selectTextFromWindow(e);
        }
    });

    //N�r musen holdes nede og k�res hen over p-tag
    greenBox.addEventListener("dragover", (e) => {

        const isThisPContenteditable = e.target.isContentEditable;
        e.preventDefault();

        //tilf�j klasse til p-tag hvis det er redigerbart
        if (isThisPContenteditable)
            e.target.classList.add('dragging');
    });

    //n�r musen forlader p-tag
    greenBox.addEventListener("dragleave", (e) => {

        const isThisPContenteditable = e.target.isContentEditable;
        e.preventDefault();

        //fjern klasse fra p-tag hvis det er redigerbart
        if (isThisPContenteditable)
            e.target.classList.remove('dragging');
    });

    greenBox.addEventListener("drop", (e) => {

        const isThisPContenteditable = e.target.isContentEditable;
        e.preventDefault();

        if (isThisPContenteditable) {

            //Returnerer ROOT som type hvis .redbox eller SPEC hvis .greenbox
            const nodeType = getType(e.target); //Always returns string "ROOT"

            //myPromise = alle billederne, omd�bt  og uploadet
            const myPromise = getPromise(e, e.target);
            myPromise.then(async (result) => {
                //billedet sendes til neo4j 
                const resultObject = await createImage(e, nodeType, result);

                //Overvej om img.id m�ske skulle s�ttes her med setBoxId


                //!!No relationship or ass node is created for images at the moment. "markNode" is undefined in this context
                //await createRelation(markNode.getAttribute("data-nodeId"), e.target.getAttribute("data-nodeId"), "Spec");
                //assNodeCreation(resultObject, markNode);
            }).catch(function (error) {
                console.error(error);
            });

            //Efter drop skal class fjernes
            e.target.classList.remove('dragging');
        }
    });

    greenBox.addEventListener('click', async (e) => LastClicked(e))

    var parentElement = document.querySelector(`[data-nodeId="${domElement.getAttribute("data-nodeId")}"]`);
    drawSpeechBubble(parentElement, greenBox, resultObject);

    //Flyt mark�r til den nyoprettede gr�nne boks
    greenBox.focus()

    //sendSpecNode(resultObject);
}


//Opret bl� boks
function createbluebox(outputNode, parent) {
    //Outputboks �BNES
    let bluebox = document.createElement("p");
    //Defin�r tekstfeltet i view
    $(bluebox).attr("class", "bluebox");
    $(bluebox).attr("contenteditable", "true");
    bluebox.innerText = outputNode.node[0].nodeName;

    //Markering i bluebox
    bluebox.addEventListener("mouseup", (e) => {
        if (window.getSelection().toString() !== "") {
            selectTextFromWindow(e);
        }

        //Fjern tom greenbox
        if (e.currentTarget.nextElementSibling != null && e.currentTarget.nextElementSibling.childNodes != null && e.currentTarget.nextElementSibling.childNodes[0].parentElement.className != "bluebox") {
            if (e.currentTarget.nextElementSibling.childNodes[1].innerText == "") {
                e.currentTarget.nextElementSibling.remove();
            }
        }
    });

    bluebox.addEventListener('click', async (e) => LastClicked(e))


    if (outputNode.element.element) { //Hvis det er en SPEC der har ID
        $(bluebox).insertAfter(outputNode.element.element);
    }
    else { //Hvis det er den f�rste ny kontekstnode / ROOT
        $(bluebox).insertAfter(outputNode.element);
    }

    outputNode.element = bluebox;
    setBoxId(outputNode);

}

//Opret lysebl� forslagsboks under outputboksen
function createlightbluebox(outputNode) {
    //forslagsbox �BNES
    const lightbluebox = document.createElement("p");
    //Defin�r tekstfeltet i view
    $(lightbluebox).attr("class", "lightbluebox");
    $(lightbluebox).attr("contenteditable", "true");
    lightbluebox.innerText = "Hvis der skulle udf�res en handling baseret p� din uddybning og outputtet hvad skulle det s� v�re?";

    lightbluebox.addEventListener("focus", (e) => RemoveTextcontent(e), { once: true })

    //Create rootnode in neo4j on unfocus
    async function sendForslag(e) {

        //if (e.target.innerText == "Hvis der skulle udf�res en handling baseret p� din uddybning og outputtet hvad skulle det s� v�re?") {
        //    RemoveTextcontent(e);
        //}
        fjernMarkTag(e);
        if (e.currentTarget.innerText != "" && e.currentTarget.id == "") {
            e.currentTarget.removeAttribute("contenteditable");
            await rootNodeCreation(e);
        }
    };
    lightbluebox.addEventListener("focusout", async (e) => await sendForslag(e));
    lightbluebox.addEventListener("selectstart", async (e) => await sendForslag(e));

    $(lightbluebox).insertAfter(outputNode.element.parentElement.lastElementChild);
    outputNode.element = lightbluebox;
}

async function LastClicked(e) {
    //f� den sidstclikkede divs ID        
    if (e.target.className == "greenbox" || e.target.className == "redbox" || e.target.className == "bluebox") {
        var nodeClicked = e.target.getAttribute("data-nodeId");
    }
    if (nodeClicked != null) {
        var lastclicked = nodeClicked.toString();
    }
    console.log("Du har sidst klikket p� " + lastclicked);
    $("#ClickedNode").html(lastclicked)

    //Find den klikkede node i neo4j og k�r FindASStoRElateTo p� deres n�rmeste SPEC
    if (Number.isInteger(parseInt(lastclicked))) {

        var resultObject = await FindNode(e, lastclicked)

        var assToRelateTo = await findAssToRelateTo(resultObject.node[0].nodeId);

        if (resultObject.node[0] != null && assToRelateTo.node.length > 0) {
            for (let i = 0; i < assToRelateTo.node.length; i++) {

                await createRelation(resultObject.node[0].nodeId, assToRelateTo.node[i].nodeId, "Ass");
            }
        }

    }
}

//Opret marknode i UI
function surroundSelectedTextWithMarkTag(resultObject) {
    var selection = window.getSelection();
    var selectionRange = selection.getRangeAt(0);
    console.log(selectionRange)
    var newMarkElement = document.createElement("mark");
    resultObject.element = newMarkElement;
    selectionRange.surroundContents(newMarkElement);
    return resultObject;
}

//Fjerner mark-tag fra UI
function fjernMarkTag(event) {

    if (event.currentTarget.childElementCount > 0) {
        if (event.target.matches("mark")) {
            let teksten = event.currentTarget.textContent;
            let thismark = event.target;
            thismark.remove();
            event.currentTarget.innerText = teksten;
        } else {
            let marktag = document.querySelector(`[data-nodeId="${event.target.getAttribute("data-nodeId")}"]`)?.getElementsByTagName("mark");
            while (marktag.length) {
                let parent = marktag[0].parentNode;

                while (marktag[0].firstChild) {
                    parent.insertBefore(marktag[0].firstChild, marktag[0]);
                }

                parent.removeChild(marktag[0]);

            }
        }

        event.currentTarget.normalize();

    }

}

//Opret en taleboblehale der g�r fra uddybningstekstfeltet til den mark-opm�rkningen som udl�ste (uddybnings)tekstfeltet
function drawSpeechBubble(domElement, greenBox, resultObject) {
    //Lav en omgivende ramme
    var svgFrame = document.createElement("div");
    //Defin�r det der skal inds�ttes
    var svgHtml = "<svg><polyline id='pilTilMark" + resultObject.node[0].nodeId + "' class='pil' points = '' /> <polyline id='bundTilPil" + resultObject.node[0].nodeId + "' class='pil' points='' /> </svg > ";
    //inds�t element i omgivende ramme
    svgFrame.innerHTML = svgHtml;
    //inds�t p-elementet fra LavEnUddybningsboks til denne svg
    svgFrame.appendChild(greenBox);
    //Hvis markering foreg�r i redbox
    if (domElement.parentElement == $("p.redbox")) {
        document.getElementsByClassName("greenbox").append(svgFrame);
    } else {
        $(svgFrame).insertAfter(domElement);
    }
    //Find markeringens x og y-koordinater
    var selection = document.getElementById(resultObject.node[0].nodeId).getBoundingClientRect();
    //Find uddybningsfeltets x og y-koordinator
    var specification = greenBox.getBoundingClientRect();

    var arrow = document.getElementById("pilTilMark" + resultObject.node[0].nodeId);
    var bottomArrow = document.getElementById("bundTilPil" + resultObject.node[0].nodeId);

    var coordinatesArrowLeft = parseInt(specification.width / 4) + "," + parseInt(21); //F�rst s�t: x,y for tekstboks overkant
    var coordinatesArrowRight = parseInt(specification.width / 3) + "," + parseInt(21); //Tredje s�t: x,y for tekstboks overkant
    var coordinatesOfArrowTip = parseInt((specification.x + (selection.right / 2))) + "," + parseInt(-30); //Andet s�t: x,y for mark-tag

    //Placering af taleboblepilens to ben og spids
    arrow.setAttribute("points", coordinatesArrowLeft + " " + coordinatesOfArrowTip + " " + coordinatesArrowRight + " " + coordinatesArrowLeft);

    var coordinatesBottomRight = parseInt((specification.width - 1) / 3) + "," + parseInt(21);
    bottomArrow.setAttributeNS(null, "points", coordinatesArrowLeft + " " + coordinatesBottomRight);
    bottomArrow.setAttributeNS(null, "style", "stroke:#90ee90;stroke-width:1.5;stroke-linecap:round");
}

//Opret Outputboks i neo4j
async function CreateOutput(fromResultObject) {

    //V�lg hvilken node fra databasen der skal dukke op
    var chosenNode = await chooseOutputNode(fromResultObject);

    //inds�t den f�rste af de fundne i bluebox
    //Bruges ogs� af FindNode til l�bende s�gning
    if (chosenNode.node[0]) {
        createbluebox(chosenNode);
        createlightbluebox(chosenNode);
    }
}
// main.js

//#region Button Events
function NyGrundNodeKnap(e) {
    console.log(e)
    prependRedBox()
}

function SletAlt(e) {

    console.log($(this).text());
    //Slet ALT fra databasen               
    Delete("All")

    //Slet alle bokse i samtalen
    $("#samtale").html("");

    if (document.getElementById("samtale").children.length < 1) {
        prependRedBox();
    }

}

function SletSpecifik(e) {


    //Grab the id of the span in the button SletSpecifik
    var lastClickednode = $("#ClickedNode").text();
    console.log(lastClickednode)
    //Slet specifik ID
    Delete(lastClickednode)

    //Fjern fra DOM
    document.getElementById(lastClickednode).remove();



}

//Knap der finder en node med spørgsmål (kan omkodes til at lede efter alt muligt)
async function SpmKnap(e) {

    console.log($(this).text());
    //Slet fra databasen
    //Genindlæs side        
    var noder = await FindNode(e, "spørgsmål")

    noder.node.forEach(node => createNode(node));


}
//#endregion

//#region Cookies and Storage
function gemknap() {
    GemTilLocalstorage();
}

function fjernknap() {
    FjernAltIDOM()
}

function fyldknap() {
    HentAltTilDOM()
}

function GemTilLocalstorage() {
    //variable
    var indtastninger;

    //Hent tidligere indtastninger fra denne browers localstorage
    if (localStorage.getItem("indtastninger") != null) {
        indtastninger = localStorage.getItem("indtastninger");
    }
    //Hent JSON
    indtastninger = JSON.parse(localStorage.getItem("indtastninger"));
    //Skriv til localstorage
    var helehjemmesiden = document.getElementById("samtale").innerHTML;
    localStorage.setItem("indtastninger", helehjemmesiden);
    //Skriv JSON
    localStorage.setItem("indtastninger", JSON.stringify(helehjemmesiden));
    //Erstat HTML med det fra localstorage
}

function SkrivCookie(key, value) {
    //Skriv en Cookie med brugerens navn i
    //key="brugernavn" og value="Villads"    
    document.cookie = key + "=" + value

    //omformater cookie-streng til objekt kaldet "bruger"
    var smaakager = document.cookie
        .split(';')
        .map(cookie => cookie.split('='))
        .reduce((accumulator, [key, value]) =>
            ({ ...accumulator, [key.trim()]: decodeURIComponent(value) }),
            {});

    //Tjek om der er en cookie med brugernavn allerede
    console.log(smaakager.brugernavn)
}

function SkrivLocalStorage(key, value) {
    //Skriv et localStorage-par med brugerens navn i
    //key="brugernavn" og value="Villads"    
    localStorage.setItem(key, value);

    console.log(localStorage.getItem(key));
}

function FjernLocalStorage(key) {

    //Skriv et localStorage-par med brugerens navn i
    //key="brugernavn" og value="Villads"    
    localStorage.removeItem(key);

    console.log("Tjek Application --> LocalStorage for om " + key + " er væk");
}
//#endregion

function FjernAltIDOM() {
    //Fjerner indholdet af samtale-div'en og laver et nyt p.redbox
    document.getElementById("samtale").innerHTML =
        "<p class='redbox' contenteditable='true'>Skriv noget nyt..</p>";
}


function HentAltTilDOM() {
    document.getElementById("samtale").innerHTML = JSON.parse(localStorage.getItem(
        "indtastninger"));
}


function logSelection() {
    console.log(document.getSelection().toString());
}

const _blackListedCharacters =
    [
        "\\",
        ".",
        ",",
        "+",
        "*",
        "?",
        "[",
        "^",
        "]",
        "$",
        "(",
        ")",
        "{",
        "}",
        "=",
        "!",
        "<",
        ">",
        "|",
        ":",
        "-",
        "\"",
        "\'",
        "#",
        "£",
        "€",
        "¤",
        ";",
        "_"
    ];

function RemoveTextcontent(event) {
    event.currentTarget.innerText = "";
}

//#region NodeCreation
//Opret rodnode i UI
async function rootNodeCreation(e) {
    var resultObject = await createRootNode(e);
    setBoxId(resultObject);
    var assToRelateTo = await findAssToRelateTo(resultObject.node[0].nodeId);

    if (resultObject.node[0] != null && assToRelateTo.node.length > 0) {
        for (let i = 0; i < assToRelateTo.node.length; i++) {
            await createRelation(resultObject.node[0].nodeId, assToRelateTo.node[i].nodeId, "Ass");
        }
    }

}

//Opret marknode i UI 
async function markNodeCreation(selectedText, domElement) {

    //console.log(domElement)
    var selOffsets = getSelectionCharacterOffsetWithin(domElement);
    var selectionStart = selOffsets.start;
    var selectionEnd = selOffsets.end;

    //Resultobject indeholder: element og node
    var resultObject = await createMarkNode(selectedText, domElement);

    var selectionObject = surroundSelectedTextWithMarkTag(resultObject);
    //Sætter denne ID på mark-tag?
    setBoxId(selectionObject);




    //domelement er boksen den er lavet i, ROOT eller SPEC.
    //selectionobject er markeringen
    await createRelation(domElement.getAttribute("data-nodeId"), selectionObject.node[0].nodeId, "Mark");

    await mergeMarkNodes(selectedText, domElement, selectionStart, selectionEnd);

    //Kør FindASStoRelateTo for denne node
    var assToRelateTo = await findAssToRelateTo(domElement.getAttribute("data-nodeId"))

    //Kører AssToRelateTo-noderne igennem og laver relationer mellem disse ASS og den nuværende node.
    if (domElement.getAttribute("data-nodeId") != null && assToRelateTo.node.length > 0) {
        for (let i = 0; i < assToRelateTo.node.length; i++) {

            await createRelation(domElement.getAttribute("data-nodeId"), assToRelateTo.node[i].nodeId, "Ass");
        }
    }

    createGreenBox(domElement, selectionObject);
}

//Opret specnode i UI 
async function specNodeCreation(e, markNodeOrigin) {
    var resultObject = await createSpecNode(e.currentTarget);
    setBoxId(resultObject);
    await createRelation(markNodeOrigin.node[0].nodeId, resultObject.node[0].nodeId, "Spec");
    var assToRelateTo = await findAssToRelateTo(resultObject.node[0].nodeId);
    if (resultObject.node[0] != null && assToRelateTo.node.length > 0) {
        for (let i = 0; i < assToRelateTo.node.length; i++) {

            await createRelation(resultObject.node[0].nodeId, assToRelateTo.node[i].nodeId, "Ass");
        }
    }
    return resultObject;
}

//Opret assnode i neo4j
async function assNodeCreation(fromResultObject, markResultObject) {

    var resultObject = await createAssNode(fromResultObject.element, markResultObject.element.innerText);

    await createRelation(fromResultObject.node[0].nodeId, resultObject.node[0].nodeId, "Ass");

    CreateOutput(fromResultObject);
}

//#endregion

//#region SelectionManipulation
//Find range fra UI
function getSelectionCharacterOffsetWithin(element) {

    console.log(element)
    var start = 0;
    var end = 0;
    var doc = element.ownerDocument || element.document;
    var win = doc.defaultView || doc.parentWindow;
    var sel;

    if (typeof win.getSelection != "undefined") {
        sel = win.getSelection();
        if (sel.rangeCount > 0) {
            var range = win.getSelection().getRangeAt(0);
            var preCaretRange = range.cloneRange();
            preCaretRange.selectNodeContents(element);
            preCaretRange.setEnd(range.startContainer, range.startOffset);
            start = preCaretRange.toString().length;
            preCaretRange.setEnd(range.endContainer, range.endOffset);
            end = preCaretRange.toString().length;
        }
    } else if ((sel = doc.selection) && sel.type != "Control") {
        var textRange = sel.createRange();
        var preCaretTextRange = doc.body.createTextRange();
        preCaretTextRange.moveToElementText(element);
        preCaretTextRange.setEndPoint("EndToStart", textRange);
        start = preCaretTextRange.text.length;
        preCaretTextRange.setEndPoint("EndToEnd", textRange);
        end = preCaretTextRange.text.length;
    }
    return {
        start: start,
        end: end
    };

}

//Find bogstav før markering fra UI
function getCharacterPrecedingSelection(containerEl) {
    var precedingChar = "",
        sel,
        range,
        precedingRange;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.rangeCount > 0) {
            range = sel.getRangeAt(0).cloneRange();
            range.collapse(true);
            range.setStart(containerEl, 0);
            precedingChar = range.toString().slice(-1);
            if (_blackListedCharacters.includes(precedingChar)) {
                precedingChar = "";
            }
        }
    } else if ((sel = document.selection) && sel.type !== "Control") {
        range = sel.createRange();
        precedingRange = range.duplicate();
        precedingRange.moveToElementText(containerEl);
        precedingRange.setEndPoint("EndToStart", range);
        precedingChar = precedingRange.text.slice(-1);
        if (_blackListedCharacters.includes(precedingChar)) {
            precedingChar = "";
        }

    }
    return precedingChar;
}


//Find bogstav efter markering fra UI
function getCharacterSucceedingSelection(containerEl) {
    var succeedingChar = "",
        sel,
        range,
        succeedingRange;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.rangeCount > 0) {
            range = sel.getRangeAt(0).cloneRange();
            range.collapse(false);
            range.setEnd(containerEl, containerEl.childNodes.length);
            succeedingChar = range.toString().charAt(0);
            if (_blackListedCharacters.includes(succeedingChar)) {
                succeedingChar = "";
            }
        }
    } else if ((sel = document.selection) && sel.type != "Control") {
        range = sel.createRange();
        succeedingRange = range.duplicate();
        succeedingRange.moveToElementText(containerEl);
        succeedingRange.setEndPoint("EndToStart", range);
        succeedingChar = succeedingRange.text.slice(-1);
        if (_blackListedCharacters.includes(succeedingChar)) {
            succeedingChar = "";
        }
    }
    return succeedingChar;
}


//Marker tekst fra UI
function selectTextFromWindow(event) {
    var selectionObject = window.getSelection();
    var rangeObject = selectionObject.getRangeAt(0);
    console.log(domElement)
    var domElement = event.target;

    var selectedText = selectionObject.toString();

    //Find det nye startpunkt for range
    var selectedTextStartTrim = selectedText.trimStart();
    var textLengthDelta = selectedText.length - selectedTextStartTrim.length;
    var rangeStart = rangeObject.startOffset + textLengthDelta;
    rangeObject.setStart(domElement.firstChild, rangeStart);


    //Find det nye slutpunkt for range
    var selectedTextEndTrim = selectedText.trimEnd();
    textLengthDelta = selectedText.length - selectedTextEndTrim.length;
    var rangeEnd = rangeObject.endOffset - textLengthDelta;
    rangeObject.setEnd(domElement.firstChild, rangeEnd);


    if (selectionObject.type !== "Caret") {

        var precedingChar = getCharacterPrecedingSelection(domElement);
        var succeedingChar = getCharacterSucceedingSelection(domElement);

        if ((selectedText.trim() !== "") || (selectedText.trim() !== ".")) {

            markNodeCreation(selectedText, domElement, precedingChar, succeedingChar);
        };

    }
    return selectedText, domElement;
}

//Marker i billede fra UI
function selectImageFromWindow(event) {
    //modtag markeringen af billedet (indtil vi får frihåndsmarkering implementeret bruger jeg bare SRC)
    var imageFileName = event.currentTarget.childNodes[0].attributes.src.nodeValue;
    //Fjern upload-mappen fra src-strengen
    //var imageFileNameWithoutDirectory = imageFileName.replace('upload/', ' ')
    const imageFileNameWithoutDirectory = imageFileName.split('/').pop()
    console.log(imageFileNameWithoutDirectory)
    //Hent NodeID fra forælder p.Redbox og sæt på img
    const parentNodeID = event.currentTarget.getAttribute("data-nodeId");



    //fjern NodeID fra forældernode, hvis forælderen ikke allerede HAR givet sit ID til img (den her skal laves om)
    if (parentNodeID) {
        event.currentTarget.removeAttribute('data-nodeId')
        //tilføj til barn
        var imgNode = event.currentTarget.childNodes[0];
        console.log(imgNode)
        imgNode.setAttribute("data-nodeId", parentNodeID);
    }

    var domElement = event.target;


    //Opret MARK baseret på filnavn
    markNodeCreation(imageFileNameWithoutDirectory, domElement, ' ', ' ');


    return imageFileNameWithoutDirectory, domElement;
}

//#endregion

$(function () {

    SkrivCookie("brugernavn", "Villads");
    SkrivLocalStorage("brugernavn", "Villads");
    FjernLocalStorage("brugernavn");

    //Indsæt kun ren tekst
    document.body.addEventListener("paste", function (e) {
        e.preventDefault();
        var text = e.clipboardData.getData("text/plain");
        document.execCommand("insertHTML", false, text);
    });

    //Tæl tegn indsat og mellemrum i den indeværende p-tag
    //$('body').on("paste", "p", function (event) {

    //    var tekst = $(this).text();
    //    var antaltegn = tekst.length;

    //    if (parseInt(antaltegn) < 255) {
    //        console.log("antal tegn: " + antaltegn);
    //    }
    //    else {
    //        console.log("Der må kun sendes 250 tegn over URL: " + antaltegn);
    //        tekst = truncate(tekst, 250)
    //        $(this).text(tekst)
    //    }

    //});

    ////Forkort tekst 
    //function truncate(str, maxlength) {
    //    return (str.length > maxlength) ?
    //        str.slice(0, maxlength - 1) + '…' : str;
    //}


    //Forhindr menu i at dukke op når man har selecteret
    document.addEventListener("contextmenu", e => e.preventDefault());
    //Forhindr træk i skærm
    document.addEventListener("touchmove", e => e.preventDefault());

    testConnection();

    buildHistory();

    //Opret en rød boks hvis der ikke er nogen
    if (document.getElementById("samtale").children.length < 1) {
        appendRedBox();
    }
});
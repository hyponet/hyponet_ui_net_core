//api.villadsclaes.dk er DNS-viderestillet til 80.197.255.33:443. Det er den Offentlige adresse p� dette projekt n�r det er udgivet
//var _adresse = "https://api.villadsclaes.dk/"
//aura.villadsclaes.dk er p� Simply.com og bruger en hostet Neo4j-database p� AuraDB
//var _adresse = "https://aura.villadsclaes.dk/"
//localhost er udviklingsadressen n�r man arbejder p� dette projekt
//var _adresse = "http://localhost:50546/";
var _baseUrl = "https://localhost:44380/";
//var _adresse = "https://localhost:5001/";


//kald neo4j gennem API

async function ajaxRequest(settings, htmlElement) {
    var resultObject = {};
    await $.ajax(settings).done(async function (response) {
        let parsedJson = await JSON.parse(response);
        resultObject = {
            node: parsedJson,
            element: htmlElement
        };
        //console.log(resultObject);
    });
    return resultObject;
}

async function SendRequest(requestUrl, requestParameters, method, htmlElement) {
    var settings = {
        "url": requestUrl,
        "method": method,
        "timeout": 0,
        "headers": {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        "data": requestParameters
    };
    var response = await ajaxRequest(settings, htmlElement);
    return response;
}

//Bekr�fter til consol at der er forbindelse til hyponet_Api
async function testConnection() {
    //var test = "Hyponet-UI lavet med javascript";
    var apiEndpointUrl = _baseUrl + "Node/TestConnection/";
    var method = "GET";
    var response = await SendRequest(apiEndpointUrl, {}, method, {});
    console.log(apiEndpointUrl);

    return response;

}

//Henter noder fra neo4j
async function getHistory() {
    var apiEndpointUrl = _baseUrl + "Node/GetHistory";
    var method = "GET";
    var nodeResult = await SendRequest(apiEndpointUrl, {}, method, {});

    return nodeResult;

}

async function buildHistory() {

    var noder = await getHistory();

    noder.node.forEach(node => createNode(node));
}

//Find en node p� ID eller anden krav
async function FindNode(e, search) {
    var method = "POST";
    var apiEndpointUrl = _baseUrl + "Node/FindNode";

    if (Number.isInteger(parseInt(search))) {
        var requestParameters = {
            "search": search
        };
    }
    if (isNaN(search)) {
        var requestParameters = {
            "search": search
        };
    }

    var nodeResult = await SendRequest(apiEndpointUrl, requestParameters, method, e.currentTarget);
    return nodeResult;
};

//Opret assrel i neo4j (fromNode = SPEC/ROOT)
async function findAssToRelateTo(fromNodeId) {
    var method = "POST";
    var apiEndpointUrl = _baseUrl + "Node/FindAssToRelateTo";
    var requestParameters = {
        "id": fromNodeId.toString()
    };

    var nodeResult = await SendRequest(apiEndpointUrl, requestParameters, method, {});
    return nodeResult;
}

//Opret rodnode i neo4j
async function createRootNode(e) {
    var nodeType = "ROOT";
    var method = "POST";
    var encodedString = e.target.innerText;
    var apiEndpointUrl = _baseUrl + "Node/Create";
    var requestParameters = {
        "name": encodedString,
        "type": nodeType
    };
    var nodeResult = await SendRequest(apiEndpointUrl, requestParameters, method, e.currentTarget);
    return nodeResult;
};

//Opret marknode i neo4j
async function createMarkNode(selectedText, domElement) {

    var nodeType = "MARK";
    var method = "POST";
    var selOffsets = getSelectionCharacterOffsetWithin(domElement);
    var start = selOffsets.start;
    var end = selOffsets.end;

    var postChar = getCharacterSucceedingSelection(domElement);
    var preChar = getCharacterPrecedingSelection(domElement);

    var encodedString = $.trim(selectedText);
    var urlPreChar = preChar;
    var urlPostChar = postChar;
    var apiEndpointUrl = _baseUrl + "Node/CreateMarkNode";

    var requestParameters = {
        "name": encodedString,
        "type": nodeType,
        "rangeStart": start ?? 0,
        "rangeEnd": end ?? 0,
        "preceedingChar": urlPreChar ?? "",
        "succeedingChar": urlPostChar ?? ""
    };
    var nodeResult = await SendRequest(apiEndpointUrl, requestParameters, method, domElement);

    return nodeResult;
}

//Opret specnode i neo4j
async function createSpecNode(domElement) {
    var nodeType = "SPEC";
    var method = "POST";
    var encodedString = $.trim(domElement.innerText);
    var apiEndpointUrl = _baseUrl + "Node/Create";
    var requestParameters = {
        "name": encodedString,
        "type": nodeType
    };

    var nodeResult = await SendRequest(apiEndpointUrl, requestParameters, method, domElement);
    return nodeResult;
}

//Opret en assnode i neo4j
async function createAssNode(fromElement, selectedText) {
    var nodeType = "ASS";
    var method = "POST";
    var encodedString = $.trim(selectedText);
    var apiEndpointUrl = _baseUrl + "Node/Create";
    var requestParameters = {
        "name": encodedString,
        "type": nodeType
    };
    var resultObject = await SendRequest(apiEndpointUrl, requestParameters, method, fromElement);

    return resultObject;
};

//Opret relation i neo4j
async function createRelation(fromNodeId, toNodeId, relationType) {
    var apiEndpointUrl = _baseUrl + "Relation/Create";
    var method = "POST";
    var requestParameters = {
        "fromNodeId": fromNodeId,
        "toNodeId": toNodeId,
        "relationType": relationType
    };
    var nodeResult = await SendRequest(apiEndpointUrl, requestParameters, method, {});
    return nodeResult;
}

//Forhindr dubletter af marknode i neo4j
async function mergeMarkNodes(selectedText, domElement, selectionStart, selectionEnd) {
    var nodeType = "MARK";
    var encodedString = $.trim(selectedText);
    var apiEndpointUrl = _baseUrl + "Node/MergeMarkNodes";
    var method = "POST";
    var requestParameters = {
        "name": encodedString,
        "type": nodeType,
        "rangeStart": selectionStart,
        "rangeEnd": selectionEnd,
        "parentId": domElement.getAttribute("data-nodeId")
    };
    var nodeResult = await SendRequest(apiEndpointUrl, requestParameters, method, domElement);
    return nodeResult;
}

//Udv�lg node fra neo4j til bluebox
async function chooseOutputNode(domElement) {
    var apiEndpointUrl = _baseUrl + "Node/ChooseOutputNode/" + domElement.node[0].nodeId;
    var method = "GET";
    var nodeResult = await SendRequest(apiEndpointUrl, {}, method, domElement);
    return nodeResult;
};

//Slet alt i neo4j
async function Delete(scopeToDelete) {
    //Her har vi hardcodet "all" men der kunne sendes et specifikt id med eller en anden sletningsordre, s� l�nge den er string
    var encodedString = scopeToDelete.toString();
    var apiEndpointUrl = _baseUrl + "Node/Delete";
    var method = "POST";

    var requestParameters = {
        "scope": encodedString
    };
    var nodeResult = await SendRequest(apiEndpointUrl, requestParameters, method, {});

    //Slet fra UI div#samtale
    //document.getElementById("samtale").innerHTML = "";

    return nodeResult;
};